package model;

import java.util.Objects;

public class Bicycle {
    private Long id;
    private Integer maxSpeed;
    private String name;
    private Double wheelDiametr;

    public Bicycle() { }

    public Bicycle(Long id, Integer maxSpeed, String name, Double wheelDiametr) {
        this.id = id;
        this.maxSpeed = maxSpeed;
        this.name = name;
        this.wheelDiametr = wheelDiametr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bicycle bicycle = (Bicycle) o;
        return id.equals(bicycle.id) &&
                maxSpeed.equals(bicycle.maxSpeed) &&
                name.equals(bicycle.name) &&
                wheelDiametr.equals(bicycle.wheelDiametr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, maxSpeed, name, wheelDiametr);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWheelDiametr() {
        return wheelDiametr;
    }

    public void setWheelDiametr(Double wheelDiametr) {
        this.wheelDiametr = wheelDiametr;
    }
}
