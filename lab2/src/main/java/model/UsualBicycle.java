package model;

import java.util.Objects;

public class UsualBicycle extends  Bicycle {
    private boolean amortization;

    public UsualBicycle(){ }

    public UsualBicycle(Long id, Integer maxSpeed, String name, Double wheelDiametr, boolean amortization) {
        super(id, maxSpeed, name, wheelDiametr);
        this.amortization = amortization;
    }

    public boolean isAmortization() {
        return amortization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UsualBicycle that = (UsualBicycle) o;
        return amortization == that.amortization;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), amortization);
    }

    public void setAmortization(boolean amortization) {
        this.amortization = amortization;
    }
}
