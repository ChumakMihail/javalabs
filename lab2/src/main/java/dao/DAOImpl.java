package dao;

import model.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.sql.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class DAOImpl<T> implements IDAOImpl<T> {
    Class<T> clazz;
    public  DAOImpl(Class<T> incClazz) { this.clazz = incClazz; }
    Connection connection;
    PreparedStatement statement;

    @Override
    public T getEntity(Long id) {
        try {
            //getting table name
            String nameOfClass = clazz.getName().substring(clazz.getName().lastIndexOf('.') + 1);
            String table = nameOfClass;
            if(nameOfClass.equals("Bicycle") || nameOfClass.equals("MountainBicycle") || nameOfClass.equals("UsualBicycle")) table = "Bicycle";

            //creating new instance and getting all fields
            Constructor<T> ctor = clazz.getConstructor();
            Object obj = ctor.newInstance();

            List<Field> listOfFields = getAllFields(new LinkedList<Field>(), clazz);
            List<String> listOfIncomingValues = new ArrayList<String>();

            //connecting and executing query
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TestDB","postgres", "12345");
            statement = connection.prepareStatement("SELECT * FROM "+ table +" WHERE id = ? ");
            statement.setInt(1,Integer.parseInt(id.toString()));
            ResultSet res = statement.executeQuery();

            //getting our object and filling fields
            if(res.next()) {
                Method m;
                for(Field field : listOfFields) {
                    listOfIncomingValues.add(res.getString(field.getName().substring(field.getName().lastIndexOf('.') + 1)));
                }
                for (int i = 0; i < listOfFields.size(); i++) {
                    Field f = listOfFields.get(i);
                    m = clazz.getMethod("set" + f.getName().substring(0,1).toUpperCase() + f.getName().substring(1), f.getType());
                    m.invoke(obj, toObject(f.getType(), listOfIncomingValues.get(i)));
                }
                return (T)obj;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.toString());
                e.printStackTrace();
            }
        }
        return null ;
    }

    @Override
    public List<T> getEntitiesList() {
        try {
            //getting table name
            String nameOfClass = clazz.getName().substring(clazz.getName().lastIndexOf('.') + 1);
            String table = nameOfClass;
            if(nameOfClass.equals("Bicycle") || nameOfClass.equals("MountainBicycle") || nameOfClass.equals("UsualBicycle")) table = "Bicycle";

            //creating new instance and getting all fields
            Constructor<T> ctor = clazz.getConstructor();

            List<Field> listOfFields = getAllFields(new LinkedList<Field>(), clazz);
            List<String> listOfIncomingValues = new ArrayList<String>();

            //connecting and executing query
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TestDB","postgres", "12345");
            if(table.equals("Bicycle")){
                statement = connection.prepareStatement("SELECT * FROM "+ table + " WHERE bicycletype=?" );
                statement.setString(1,nameOfClass);
            }

            else statement = connection.prepareStatement("SELECT * FROM "+ table);

            ResultSet res = statement.executeQuery();

            //getting our object and filling fields
            List<T> resultList = new ArrayList<T>();
            while (res.next()) {
                Method m;
                Object obj = ctor.newInstance();
                listOfIncomingValues.clear();
                for(Field field : listOfFields) {
                    listOfIncomingValues.add(res.getString(field.getName().substring(field.getName().lastIndexOf('.') + 1)));
                }
                for (int i = 0; i < listOfFields.size(); i++) {
                    Field f = listOfFields.get(i);
                    m = clazz.getMethod("set" + f.getName().substring(0,1).toUpperCase() + f.getName().substring(1), f.getType());
                    m.invoke(obj, toObject(f.getType(), listOfIncomingValues.get(i)));
                }
                resultList.add((T)obj);
            }
            return resultList;
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.toString());
                e.printStackTrace();
            }
        }
        return null ;
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        return fields;
    }

    private static Object toObject( Class c, String val ) {
        if( Boolean.class == c || Boolean.TYPE == c){
            if (val.equals("f")) return false;
            else return true;
        }
        if( Byte.class == c || Boolean.TYPE == c) return Byte.parseByte( val );
        if( Short.class == c || Boolean.TYPE == c) return Short.parseShort( val );
        if( Integer.class == c || Boolean.TYPE == c) return Integer.parseInt( val );
        if( Long.class == c || Boolean.TYPE == c) return Long.parseLong( val );
        if( Float.class == c || Boolean.TYPE == c) return Float.parseFloat( val );
        if( Double.class == c || Boolean.TYPE == c) return Double.parseDouble( val );
        return val;
    }
}
