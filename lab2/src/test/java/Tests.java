import jdk.nashorn.internal.runtime.BitVector;
import model.*;

import java.util.*;

import dao.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {
    DAO dao = new DAO();

    @Test
    public void getOwnerById_ShouldReturnOwnerByIncId() {
        //arrange
        Owner exp = new Owner(2l, "Mike", 20.0);
        //act
        Owner res = dao.getOwner(2l);
        //asssert
        assertEquals(exp, res);
    }

    @Test
    public void getBycicleById_ShouldReturnBycicleByIncId() {
        //arrange
        Bicycle exp = new Bicycle(1l,100,"Cycle 1", 22.0);
        //act
        Bicycle res = dao.getBicycle(1l);
        //asssert
        assertEquals(exp, res);
    }

    @Test
    public void getMountainBicycleById_ShouldReturnMountainBycicleIncId() {
        //arrange
        MountainBicycle mountainBicycleExp = new MountainBicycle(3l, 120, "Cycle 3", 25.0,10);
        //act
        MountainBicycle res = dao.getMountainBicycle(3l);
        //asssert
        assertEquals(mountainBicycleExp, res);
    }

    @Test
    public void getUsualBicycleById_ShouldReturnUsualBycicleIncId() {
        //arrange
        UsualBicycle usualBicycleExp = new UsualBicycle(2l, 80, "Cycle 2", 21.0, true);
        //act
        UsualBicycle res = dao.getUsualBicycle(2l);
        //asssert
        assertEquals(usualBicycleExp, res);
    }

    @Test
    public void getOwnersList_ShouldReturnAllOwners() {
        //arrange
        List<Owner> ownerExp = new ArrayList<Owner>();
        ownerExp.add(new Owner(2l, "Mike", 20.0));
        ownerExp.add(new Owner(3l, "Oleg", 18.0));
        ownerExp.add(new Owner(4l, "Victor", 25.0));
        //act
        List<Owner> res = dao.getOwnersList();
        //asssert
        assertEquals(ownerExp.size(), res.size());
        for (int i = 0; i < ownerExp.size(); i++) {
            assertEquals(ownerExp.get(i), res.get(i));
        }
    }

    @Test
    public void getBicycleList_ShouldReturnAllBicycles() {
        //arrange
        List<Bicycle> bicyclesExp = new ArrayList<Bicycle>();
        bicyclesExp.add(new Bicycle(1l, 100, "Cycle 1", 22.0));
        //act
        List<Bicycle> res = dao.getBicyclesList();
        //assert
        assertEquals(bicyclesExp.size(), res.size());
        for (int i = 0; i < bicyclesExp.size(); i++) {
            assertEquals(bicyclesExp.get(i), res.get(i));
        }
    }

    @Test
    public void getMountBicycleList_ShouldReturnAllMountBicycles() {
        //arrange
        List<MountainBicycle> mountainBicyclesExp = new ArrayList<MountainBicycle>();
        mountainBicyclesExp.add(new MountainBicycle(3l, 120, "Cycle 3", 25.0, 10));
        //act
        List<MountainBicycle> res = dao.getMountainBicyclesList();
        //assert
        assertEquals(mountainBicyclesExp.size(), res.size());
        for (int i = 0; i < mountainBicyclesExp.size(); i++) {
            assertEquals(mountainBicyclesExp.get(i), res.get(i));
        }
    }

    @Test
    public void getUsualBicycleList_ShouldReturnAllUsualBicycles() {
        //arrange
        List<UsualBicycle> usualBicyclesExp = new ArrayList<UsualBicycle>();
        usualBicyclesExp.add(new UsualBicycle(2l, 80, "Cycle 2", 21.0,true));
        //act
        List<UsualBicycle> res = dao.getUsualBicyclesList();
        //assert
        assertEquals(usualBicyclesExp.size(), res.size());
        for (int i = 0; i < usualBicyclesExp.size(); i++) {
            assertEquals(usualBicyclesExp.get(i), res.get(i));
        }
    }
}
