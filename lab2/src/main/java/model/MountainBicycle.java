package model;

import java.util.Objects;

public class MountainBicycle extends  Bicycle {
    private Integer speedShiftersNumber;

    public MountainBicycle() { }

    public MountainBicycle(Long id, Integer maxSpeed, String name, Double wheelDiametr, Integer speedShiftersNumber) {
        super(id, maxSpeed, name, wheelDiametr);
        this.speedShiftersNumber = speedShiftersNumber;
    }

    public Integer getSpeedShiftersNumber() {
        return speedShiftersNumber;
    }

    public void setSpeedShiftersNumber(Integer speedShiftersNumber) {
        this.speedShiftersNumber = speedShiftersNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MountainBicycle that = (MountainBicycle) o;
        return speedShiftersNumber.equals(that.speedShiftersNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), speedShiftersNumber);
    }
}
