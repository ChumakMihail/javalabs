package dao;

import model.*;
import java.util.List;

public interface IDAO {
    Bicycle getBicycle(Long id);
    List<Bicycle> getBicyclesList();

    MountainBicycle getMountainBicycle(Long id);
    List<MountainBicycle> getMountainBicyclesList();

    UsualBicycle getUsualBicycle(Long id);
    List<UsualBicycle> getUsualBicyclesList();

    Owner getOwner(Long id);
    List<Owner> getOwnersList();
}
