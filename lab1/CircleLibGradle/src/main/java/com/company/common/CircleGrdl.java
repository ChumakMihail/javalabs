package com.company.common;

import org.javatuples.Pair;

public class CircleGrdl {
    private Pair<Integer, Integer> cords;
    private int radius;

    public CircleGrdl(int x, int y, int rad) {
        cords = new Pair<Integer, Integer>(x,y);
        radius = rad;
    }
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}
