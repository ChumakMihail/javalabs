package dao;

import model.Bicycle;
import model.MountainBicycle;
import model.Owner;
import model.UsualBicycle;

import java.util.List;

public class DAO implements  IDAO{
    DAOImpl<Bicycle> BicycleDAOImpl = new DAOImpl<Bicycle>(Bicycle.class);
    DAOImpl<MountainBicycle> MountainBicycleDAOImpl = new DAOImpl<MountainBicycle>(MountainBicycle.class);;
    DAOImpl<Owner> OwnerDAOImpl = new DAOImpl<Owner>(Owner.class);;
    DAOImpl<UsualBicycle> UsualBicycleDAOImpl = new DAOImpl<UsualBicycle>(UsualBicycle.class);;

    public DAO() { }

    @Override
    public Bicycle getBicycle(Long id) { return BicycleDAOImpl.getEntity(id); }

    @Override
    public List<Bicycle> getBicyclesList() {
        return BicycleDAOImpl.getEntitiesList();
    }

    @Override
    public MountainBicycle getMountainBicycle(Long id) {
        return MountainBicycleDAOImpl.getEntity(id);
    }

    @Override
    public List<MountainBicycle> getMountainBicyclesList() {
        return MountainBicycleDAOImpl.getEntitiesList();
    }

    @Override
    public UsualBicycle getUsualBicycle(Long id) {
        return UsualBicycleDAOImpl.getEntity(id);
    }

    @Override
    public List<UsualBicycle> getUsualBicyclesList() {
        return UsualBicycleDAOImpl.getEntitiesList();
    }

    @Override
    public Owner getOwner(Long id) {
        return OwnerDAOImpl.getEntity(id);
    }

    @Override
    public List<Owner> getOwnersList() {
        return OwnerDAOImpl.getEntitiesList();
    }
}
