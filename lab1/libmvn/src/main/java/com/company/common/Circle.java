package com.company.common;

import org.javatuples.Pair;

public class Circle {
    private Pair<Integer, Integer> cords;
    private int radius;

    public Circle(int x, int y, int rad) {
        cords = new Pair<Integer, Integer>(x,y);
        radius = rad;
    }
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}
