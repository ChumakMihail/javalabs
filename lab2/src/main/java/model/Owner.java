package model;

import java.util.Objects;

public class Owner {
    private Long Id;
    private String name;
    private Double age;

    public Owner() { }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Owner owner = (Owner) o;
        return Id.equals(owner.Id) &&
                name.equals(owner.name) &&
                age.equals(owner.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, name, age);
    }

    public Owner(Long id, String name, Double age) {
        Id = id;
        this.name = name;
        this.age = age;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAge() {
        return age;
    }

    public void setAge(Double age) {
        this.age = age;
    }
}
